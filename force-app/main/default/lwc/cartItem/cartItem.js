import { LightningElement, api } from 'lwc';

export default class CartItem extends LightningElement {
    @api item;
}